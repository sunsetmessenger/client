document.addEventListener("deviceready", onDeviceReady, false);

function onDeviceReady() {
  if (cordova.platformId == 'android') {
		StatusBar.styleLightContent();
		StatusBar.backgroundColorByHexString("#303337");
  }
  window.screen.lockOrientation('portrait');
};

function logout() {
	if (confirm("Do you really want to logout from Sunset?\n\nNOTE: Please save your decryption key as backup!")) {
		var lang = localStorage.getItem("lang");
		var theme = localStorage.getItem("theme");
		localStorage.clear();
		localStorage.setItem("lang", lang);
		localStorage.setItem("theme", theme);
		localStorage.setItem("introDone", "1");
		window.location.replace("getstarted.html");
	}
}

function load(sitename, fullscreen=false) {
	if (fullscreen) {
		$("fullscreen").append('<iframe src="'+sitename+'.html"></iframe>');
		$("fullscreen > iframe > html > body").append('<header>'+header+'</header>');
	} else {
		$("main").html('<iframe src="'+sitename+'.html"></iframe>');
	}
};

function back() {
	$("fullscreen > iframe:last-child").addClass('remove');
	window.setTimeout(function(){
		$("fullscreen > iframe:last-child").remove();
	}, 200);
};

if (localStorage.getItem("theme") == "dark") {
	$('body').css('background-color', '#292A2D', 'important');
} else if (localStorage.getItem("theme") == "light") {
	$('main').css('background-color', '#fff');
}

$(document).ready(function() {

});
