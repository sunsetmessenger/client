var theme = localStorage.getItem("theme");
var lang = localStorage.getItem("lang");

var groupText;
var profileText;
var settingTexts;
var logoutText;

if (theme == "dark") {
	var nav = "dark";
} else if (theme == "light") {
	var nav = "light";
};

	if (lang == "US") {
		groupText = "New group"
		profileText = "Profile"
		settingTexts = "Settings"
		logoutText = "Logout"
		newMessageText = "New Message"
	} else if (lang == "DE") {
		groupText = "Neue Gruppe"
		profileText = "Profil"
		settingTexts = "Einstellungen"
		logoutText = "Abmelden"
		newMessageText = "Neue Nachricht"
	};

header = '<nav class="navbar navbar-expand navbar-'+nav+' bg-'+nav+'">\
		<a class="navbar-brand" href="">\
    <img src="img/logo.png" style="margin-top:-4px;" width="34" height="32">Sunset\
		</a>\
		<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">\
			<span class="navbar-toggler-icon"></span>\
		</button>\
		 <div class="collapse navbar-collapse">\
			<ul class="navbar-nav">\
				<!-- <li class="nav-item">\
					<a class="nav-link" href="#">Features</a>\
				</li>\
			</ul> -->\
		</div>\
		<ul class="navbar-nav flex-row d-sm-flex">\
			<li class="nav-item dropdown settings">\
				<a class="nav-link dropdown-toggle" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="false" aria-expanded="false">\
					<img src="img/settings.png" width="30" height="30">\
				</a>\
				<div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">\
					<a class="dropdown-item" onclick="load(\'newmsg\', false)">'+newMessageText+'</a>\
					<!-- <a class="dropdown-item disabled">'+groupText+'</a>\
					<a class="dropdown-item disabled">'+profileText+'</a> -->\
					<a class="dropdown-item" onclick="load(\'settings\', false)">'+settingTexts+'</a>\
					<a class="dropdown-item" onclick="logout()">'+logoutText+'</a>\
				</div>\
			</li>\
		</ul>\
	</nav>'

$(document).ready(function() {

	$("header").append(header);

});
