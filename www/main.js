const { app, BrowserWindow } = require('electron')

require('update-electron-app')()

let win

function createWindow () {
  win = new BrowserWindow({ width: 400, height: 720, icon: __dirname + '/icon.png' })
  win.loadFile('index.html')
  win.on('closed', () => {
    win = null
  })
}

app.on('browser-window-created',function(e,window) {
  window.setMenu(null);
});

app.on('ready', createWindow)

app.on('window-all-closed', () => {
  if (process.platform !== 'darwin') {
    app.quit()
  }
})

app.on('activate', () => {
  if (win === null) {
    createWindow()
  }
})